/**
 ******************************************************************************
 * @file    main.c
 * @author  fire
 * @version V1.0
 * @date    2024-xx-xx
 * @brief   OLED����
 ******************************************************************************
 * @attention
 *
 * ʵ��ƽ̨:Ұ�� F429 STM32 ������
 * ��̳    :http://www.firebbs.cn
 * �Ա�    :https://fire-stm32.taobao.com
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx.h"
#include "iic/bsp_iic_debug.h"
#include "oled/bsp_oled_debug.h"
#include "systick/bsp_systick.h"
#include <stdlib.h>

extern unsigned char BMP1[];


/**
 * @brief  ������
 * @param  ��
 * @retval ��
 */
int main(void)
{
    /* IIC���ų�ʼ�� */
    IIC_GPIO_Config();

    /* OLED��ʼ�� */
    OLED_Init();
	
    /* �δ�ʱ����ʼ�� */
    Systick_Init();

    while (1)
    {	
        OLED_Fill(0xFF); // ȫ������
        Delay_1ms(2000); // 2s

        OLED_Fill(0x00); // ȫ����
        Delay_1ms(2000); // 2s

        for (uint8_t i = 0; i < 4; i++)
        {
            OLED_ShowCN(22 + i * 16, 0, i); // ������ʾ����
        }
        Delay_1ms(2000);                                         // 2s
        OLED_ShowStr(0, 3, (unsigned char *)"EmbedFire Elec", 1);  // ����6*8�ַ�
        OLED_ShowStr(0, 4, (unsigned char *)"Hello EmbedFire", 2); // ����8*16�ַ�
        Delay_1ms(2000);                                         // 2*100=200s
        OLED_CLS();                                              // ����
        OLED_OFF();                                              // ����OLED����
        Delay_1ms(2000);                                         // 2s
        OLED_ON();                                               // ����OLED���ߺ���
        OLED_DrawBMP(0, 0, 128, 8, (unsigned char *)BMP1);       // ����BMPλͼ��ʾ
        Delay_1ms(2000);                                         // 2*100=200s
    }
}


/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
