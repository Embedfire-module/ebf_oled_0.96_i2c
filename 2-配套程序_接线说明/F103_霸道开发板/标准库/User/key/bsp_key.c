/**
 ******************************************************************************
 * @file    bsp_key.c
 * @author  fire
 * @version V1.0
 * @date    2024-xx-xx
 * @brief   按键应用(扫描模式)
 ******************************************************************************
 * @attention
 *
 * 实验平台:野火STM32 F103核心板
 * 论坛    :http://www.firebbs.cn
 * 淘宝    :https://fire-stm32.taobao.com
 *
 ******************************************************************************
 */

#include "key/bsp_key.h"

/*
 * @brief  配置按键用到的I/O口
 * @param  无
 * @retval 无
 */

void Key_GPIO_Config(void)
{
	  GPIO_InitTypeDef gpio_initstruct;
	
    /* 使能GPIO时钟 */
    KEY_GPIO_CLK_ENABLE(KEY1_GPIO_CLK | KEY2_GPIO_CLK, ENABLE);

    /* 初始化GPIO */

    gpio_initstruct.GPIO_Mode = GPIO_Mode_IPD; /* 按键输入检测 GPIO应配置为输入模式 */       
    gpio_initstruct.GPIO_Pin = KEY1_PIN | KEY2_PIN;
    gpio_initstruct.GPIO_Speed = GPIO_Speed_50MHz;

    GPIO_Init(KEY1_GPIO_PORT, &gpio_initstruct);
    GPIO_Init(KEY2_GPIO_PORT, &gpio_initstruct);
}

/**
 * @brief   检测是否有按键按下
 * @param   具体的端口和端口位
 *		@arg GPIOx: x可以是（A...G）
 *		@arg GPIO_PIN 可以是GPIO_PIN_x（x可以是1...16）
 *   @retval  按键的状态
 *		@arg KEY_ON:按键按下
 *		@arg KEY_OFF:按键没按下
 */
uint8_t Key_Scan(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin)
{
    /*检测是否有按键按下 */
    if (GPIO_ReadInputDataBit(GPIOx, GPIO_Pin) == KEY_ON)
    {
        /*等待按键释放 */
        while (GPIO_ReadInputDataBit(GPIOx, GPIO_Pin) == KEY_ON)
            ;
        return KEY_ON;
    }
    else
        return KEY_OFF;
}

/*********************************************END OF FILE**********************/
