/**
 ******************************************************************************
 * @file    main.c
 * @author  fire
 * @version V1.0
 * @date    2024-xx-xx
 * @brief   OLED����
 ******************************************************************************
 * @attention
 *
 * ʵ��ƽ̨:Ұ�� F103 STM32 ������
 * ��̳    :http://www.firebbs.cn
 * �Ա�    :https://fire-stm32.taobao.com
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx.h"
#include "iic/bsp_iic_debug.h"
#include "oled/bsp_oled_debug.h"
#include "systick/bsp_systick.h"
#include <stdlib.h>

extern unsigned char BMP1[];

/**
 * @brief  ������
 * @param  ��
 * @retval ��
 */
int main(void)
{
    /* ����ϵͳʱ��Ϊ72 MHz */
    SystemClock_Config();

    /* �δ�ʱ����ʼ�� */
    Systick_Init();

    /* IIC���ų�ʼ�� */
    IIC_GPIO_Config();

    /* OLED��ʼ�� */
    OLED_Init();

    while (1)
    {
        OLED_Fill(0xFF); // ȫ������
        Delay_1ms(2000); // 2s

        OLED_Fill(0x00); // ȫ����
        Delay_1ms(2000); // 2s

        for (uint8_t i = 0; i < 4; i++)
        {
            OLED_ShowCN(22 + i * 16, 0, i); // ������ʾ����
        }
        Delay_1ms(2000);                                          // 2s
        OLED_ShowStr(0, 3, (unsigned char *)"EmbedFire Elec", 1);  // ����6*8�ַ�
        OLED_ShowStr(0, 4, (unsigned char *)"Hello EmbedFire", 2); // ����8*16�ַ�
        Delay_1ms(2000);                                          // 2*100=200s
        OLED_CLS();                                               // ����
        OLED_OFF();                                               // ����OLED����
        Delay_1ms(2000);                                          // 2s
        OLED_ON();                                                // ����OLED���ߺ���
        OLED_DrawBMP(0, 0, 128, 8, (unsigned char *)BMP1);        // ����BMPλͼ��ʾ
        Delay_1ms(2000);                                          // 2*100=200s
    }
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 72000000
 *            HCLK(Hz)                       = 72000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 2
 *            APB2 Prescaler                 = 1
 *            HSE Frequency(Hz)              = 8000000
 *            HSE PREDIV1                    = 1
 *            PLLMUL                         = 9
 *            Flash Latency(WS)              = 2
 * @param  None
 * @retval None
 */
void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef clkinitstruct = {0};
    RCC_OscInitTypeDef oscinitstruct = {0};

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    oscinitstruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    oscinitstruct.HSEState = RCC_HSE_ON;
    oscinitstruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    oscinitstruct.PLL.PLLState = RCC_PLL_ON;
    oscinitstruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    oscinitstruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&oscinitstruct) != HAL_OK)
    {
        /* Initialization Error */
        while (1)
            ;
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
       clocks dividers */
    clkinitstruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    clkinitstruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    clkinitstruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    clkinitstruct.APB2CLKDivider = RCC_HCLK_DIV1;
    clkinitstruct.APB1CLKDivider = RCC_HCLK_DIV2;
    if (HAL_RCC_ClockConfig(&clkinitstruct, FLASH_LATENCY_2) != HAL_OK)
    {
        /* Initialization Error */
        while (1)
            ;
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
