/**
 ******************************************************************************
 * @file    main.c
 * @author  fire
 * @version V1.0
 * @date    2024-xx-xx
 * @brief   OLED����
 ******************************************************************************
 * @attention
 *
 * ʵ��ƽ̨:Ұ�� F407 STM32 ������
 * ��̳    :http://www.firebbs.cn
 * �Ա�    :https://fire-stm32.taobao.com
 *
 ******************************************************************************
 */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx.h"
#include "iic/bsp_iic_debug.h"
#include "oled/bsp_oled_debug.h"
#include "systick/bsp_systick.h"
#include <stdlib.h>

extern unsigned char BMP1[];

/**
 * @brief  ������
 * @param  ��
 * @retval ��
 */
int main(void)
{
    /* ����ϵͳʱ��Ϊ180MHz */
    SystemClock_Config();

    /* �δ�ʱ����ʼ�� */
    Systick_Init();

    /* IIC���ų�ʼ�� */
    IIC_GPIO_Config();

    /* OLED��ʼ�� */
    OLED_Init();

    while (1)
    {
        OLED_Fill(0xFF); // ȫ������
        Delay_1ms(2000); // 2s

        OLED_Fill(0x00); // ȫ����
        Delay_1ms(2000); // 2s

        for (uint8_t i = 0; i < 4; i++)
        {
            OLED_ShowCN(22 + i * 16, 0, i); // ������ʾ����
        }
        Delay_1ms(2000);                                          // 2s
        OLED_ShowStr(0, 3, (unsigned char *)"EmbedFire Elec", 1);  // ����6*8�ַ�
        OLED_ShowStr(0, 4, (unsigned char *)"Hello EmbedFire", 2); // ����8*16�ַ�
        Delay_1ms(2000);                                          // 2*100=200s
        OLED_CLS();                                               // ����
        OLED_OFF();                                               // ����OLED����
        Delay_1ms(2000);                                          // 2s
        OLED_ON();                                                // ����OLED���ߺ���
        OLED_DrawBMP(0, 0, 128, 8, (unsigned char *)BMP1);        // ����BMPλͼ��ʾ
        Delay_1ms(2000);                                          // 2*100=200s
    }
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 168000000
 *            HCLK(Hz)                       = 168000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            PLL_M                          = 25
 *            PLL_N                          = 336
 *            PLL_P                          = 2
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 5
 * @param  None
 * @retval None
 */
void SystemClock_Config(void)
{
    RCC_ClkInitTypeDef RCC_ClkInitStruct;
    RCC_OscInitTypeDef RCC_OscInitStruct;

    /* Enable Power Control clock */
    __HAL_RCC_PWR_CLK_ENABLE();

    /* The voltage scaling allows optimizing the power consumption when the device is
       clocked below the maximum system frequency, to update the voltage scaling value
       regarding system frequency refer to product datasheet.  */
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /* Enable HSE Oscillator and activate PLL with HSE as source */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 336;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        while (1)
        {
        };
    }

    /* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
       clocks dividers */
    RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
    {
        while (1)
        {
        };
    }

    /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
    if (HAL_GetREVID() == 0x1001)
    {
        /* Enable the Flash prefetch */
        __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
    }
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
